//
//  openIncidentView.swift
//  T02_Orange
//
//  Created by Jayanth Bujula on 11/30/18.
//  Copyright © 2018 Austin T Vannoy. All rights reserved.
//

import UIKit

class openIncidentView: UIViewController,UITableViewDataSource, UITableViewDelegate , HomeModelDelegate{
    func itemsDownloaded(incidents: [datavalues]) {
        self.incidents = incidents
        
        tableview.reloadData()
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let selectedIndexPath = tableview.indexPathForSelectedRow
        if let indexPath = selectedIndexPath {
            
            let destination = segue.destination as! OpenIncidentViewController
            destination.incidents = incidents[indexPath.row]
        }
    }
    
    
    @IBAction func refreshTable(_ sender: Any) {
        homeModel.getItems("open")
        tableview.reloadData()
    }
    var incidents = [datavalues]()
    var homeModel = HomeModel()
    @IBOutlet weak var tableview: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        homeModel.getItems("open")
        homeModel.delegate = self
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
       tableview.dataSource = self
        tableview.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return incidents.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        cell.textLabel?.text = incidents[indexPath.row].reporter_name + "    " + incidents[indexPath.row].incident_type + "    " + incidents[indexPath.row].report_date
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "OpenIncident", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
