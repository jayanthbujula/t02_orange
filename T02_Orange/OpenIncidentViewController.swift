//
//  OpenIncidentViewController.swift
//  T02_Orange
//
//  Created by Austin T Vannoy on 10/30/18.
//  Copyright © 2018 Austin T Vannoy. All rights reserved.
//

import UIKit

class OpenIncidentViewController: UIViewController {
    
    //Near Miss Variables
    @IBOutlet var incidentType : UILabel!
    @IBOutlet var incidentDate : UILabel!
    @IBOutlet var incidentLocation : UILabel!
    @IBOutlet var incidentReporter : UILabel!
    @IBOutlet var incidentReportDate : UILabel!
    @IBOutlet var potentialHarm : UILabel!
    @IBOutlet var incidentPrevention : UILabel!
    @IBOutlet var additionalInfo : UITextView!
    @IBOutlet var reportSubmitter : UILabel!
    @IBOutlet var manager : UILabel!
    
    //Injury Variables
    @IBOutlet var harmingObject : UILabel!
    @IBOutlet var locationInjury : UILabel!
    @IBOutlet var injurySeverity : UILabel!
    @IBOutlet var doctorVisited : UILabel!
    @IBOutlet var doctorContact : UILabel!
    @IBOutlet var witnesses : UILabel!
    @IBOutlet var safetyPrecautions : UITextView!
    
    //Injury Label Names
    @IBOutlet var harmingObjectLabel : UILabel!
    @IBOutlet var locationInjuryLabel : UILabel!
    @IBOutlet var injurySeverityLabel : UILabel!
    @IBOutlet var doctorVisitedLabel : UILabel!
    @IBOutlet var doctorContactLabel : UILabel!
    @IBOutlet var witnessesLabel : UILabel!
    @IBOutlet var safetyPrecautionsLabel : UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loadTable()
    }
    var incidents :(datavalues)? = nil
    
    
    let serviceURL = "http://cs.okstate.edu/~jbujula/UpdateIncident.php/"
    
    
    
    @IBAction func closeincident(_ sender: Any) {
        let alert = UIAlertController(title: "Confirm?", message: "Do you want to close the incident?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Accept", style: .default, handler: {
            action in
            //created NSURL
            let requestURL = NSURL(string: self.serviceURL)
            
            //creating NSMutableURLRequest
            let request = NSMutableURLRequest(url: requestURL! as URL)
            //setting the method to post
            request.httpMethod = "POST"
            let id = self.incidents!.id
            //creating the post parameter by concatenating the keys and values from text field
            let postParameters = "id="+id;
            //adding the parameters to request body
            request.httpBody = postParameters.data(using: String.Encoding.utf8)
            //creating a task to send the post request
            let task = URLSession.shared.dataTask(with: request as URLRequest){
                data, response, error in
                
                if error != nil{
                    return;
                }
                
                //parsing the response
                do {
                    //converting resonse to NSDictionary
                    let myJSON =  try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                    
                    //parsing the json
                    if let parseJSON = myJSON {
                        
                        //creating a string
                        var msg : String!
                        
                        //getting the json response
                        msg = parseJSON["message"] as! String?
                        
                        //printing the response
                        print(msg)
                        
                    }
                } catch {
                    print(error)
                }
                
            }
            //executing the task
            task.resume()
            
            self.performSegue(withIdentifier: "UnwindToMenuFromOpen", sender: self)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(action)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        
        
    }
    
    func loadTable()
    {
        //Load Common Details
        incidentType.text = incidents?.incident_type
        incidentDate.text = incidents?.incident_date
        incidentLocation.text = incidents?.location
        incidentReporter.text = incidents?.reporter_name
        incidentReportDate.text = incidents?.report_date
        potentialHarm.text = incidents?.potential_harm
        incidentPrevention.text = incidents?.incident_prevention
        additionalInfo.text = incidents?.additional_info
        reportSubmitter.text = incidents?.report_submitter
        manager.text = incidents?.manager_name
        
        //If Injury
        if incidents!.incident_type == "injury"
        {
            incidentType.text = "Injury"
            
            harmingObject.text = incidents?.injuring_object
            locationInjury.text = incidents?.injury_location
            injurySeverity.text = incidents?.injury_severity
            doctorVisited.text = incidents?.doctor_visited
            doctorContact.text = incidents?.doctor_contact_info
            witnesses.text = incidents?.incident_witness
            safetyPrecautions.text = incidents?.safety_procedures
            
            harmingObjectLabel.isHidden = false
            locationInjuryLabel.isHidden = false
            injurySeverityLabel.isHidden = false
            doctorVisitedLabel.isHidden = false
            doctorContactLabel.isHidden = false
            witnessesLabel.isHidden = false
            safetyPrecautionsLabel.isHidden = false
            
        }
        else
        {
            harmingObjectLabel.isHidden = true
            locationInjuryLabel.isHidden = true
            injurySeverityLabel.isHidden = true
            doctorVisitedLabel.isHidden = true
            doctorContactLabel.isHidden = true
            witnessesLabel.isHidden = true
            safetyPrecautionsLabel.isHidden = true
            
            harmingObject.text = ""
            locationInjury.text = ""
            injurySeverity.text = ""
            doctorVisited.text = ""
            doctorContact.text = ""
            witnesses.text = ""
            safetyPrecautions.text = ""
        }
    }
}
