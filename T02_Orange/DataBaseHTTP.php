<?php

// Response Array
$response = array();

if($_SERVER['REQUEST_METHOD']=='POST'){

    $date = $_POST['date'];

    // Include Operations
    require_once '../includes/DataOperations.php';

    $db = new DataOperations();

    // Add Values
    if($db->createReport($date)){
        $response['error']=false;
        $response['message']='Report Added';
    }else{

        $response['error']=true;
        $response['message']='Report Failed To Add';
    }

}else{
    $response['error']=true;
    $response['message']='Permissions Denied';
}
echo json_encode($response);