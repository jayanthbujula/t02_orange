//
//  ClosedIncidentTableViewController.swift
//  T02_Orange
//
//  Created by Jayanth Bujula on 12/1/18.
//  Copyright © 2018 Austin T Vannoy. All rights reserved.
//

import UIKit

class ClosedIncidentTableViewController: UIViewController,UITableViewDataSource, UITableViewDelegate , HomeModelDelegate{
    // stores all the table values
    var incidents = [datavalues]()
    var homeModel = HomeModel()
    // table view
    @IBOutlet weak var tableview: UITableView!
    // reload table view after getting all the values from the database table - incident
    func itemsDownloaded(incidents: [datavalues]) {
        self.incidents = incidents
        
        tableview.reloadData()
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let selectedIndexPath = tableview.indexPathForSelectedRow
        if let indexPath = selectedIndexPath {
            
            let destination = segue.destination as! ClosedIncidentViewController
            destination.incidents = incidents[indexPath.row]
        }
    }
    // refresh the table values
    @IBAction func refreshClosedIncidents(_ sender: Any) {
        homeModel.getItems("closed")
        tableview.reloadData()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        homeModel.getItems("closed")
        homeModel.delegate = self
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        tableview.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return incidents.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellBasic", for: indexPath)
        
        cell.textLabel?.text = incidents[indexPath.row].reporter_name + "    " + incidents[indexPath.row].incident_type + "    " + incidents[indexPath.row].report_date
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ClosedInfo", sender: self)
    }
   
    
}
