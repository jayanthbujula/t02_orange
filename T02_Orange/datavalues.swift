//
//  datavalues.swift
//  T02_Orange
//
//  Created by Jayanth Bujula on 12/1/18.
//  Copyright © 2018 Austin T Vannoy. All rights reserved.
//


import Foundation

struct datavalues{
    var id = ""
    var incident_type = ""
    var location = ""
    var reporter_name = ""
    var report_date = ""
    var incident_description = ""
    var potential_harm = ""
    var incident_prevention = ""
    var additional_info = ""
    var manager_name = ""
    var injuring_object = ""
    var injury_location = ""
    var injury_severity = ""
    var doctor_visited = ""
    var doctor_contact_info = ""
    var incident_witness = ""
    var safety_procedures = ""
    var incident_label = ""
    var incident_date = ""
    var report_submitter = ""
}

