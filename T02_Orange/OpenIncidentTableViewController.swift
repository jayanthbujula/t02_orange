//
//  OpenIncidentTableViewController.swift
//  T02_Orange
//
//  Created by Jayanth Bujula on 12/1/18.
//  Copyright © 2018 Austin T Vannoy. All rights reserved.
//

import UIKit

class OpenIncidentTableViewController: UIViewController,UITableViewDataSource, UITableViewDelegate , HomeModelDelegate{
    // stores all the table values
    var incidents = [datavalues]()
    var homeModel = HomeModel()
    // refresh the table values
    func itemsDownloaded(incidents: [datavalues]) {
        self.incidents = incidents
        
        tableview.reloadData()
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let selectedIndexPath = tableview.indexPathForSelectedRow
        if let indexPath = selectedIndexPath {
            
            let destination = segue.destination as! OpenIncidentViewController
            destination.incidents = incidents[indexPath.row]
        }
    }
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBAction func refreshTable(_ sender: Any) {
        homeModel.getItems("open")
        tableview.reloadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        homeModel.getItems("open")
        homeModel.delegate = self
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.dataSource = self
        tableview.delegate = self
        tableview.reloadData()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return incidents.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        cell.textLabel?.text = incidents[indexPath.row].reporter_name + "    " + incidents[indexPath.row].incident_type + "    " + incidents[indexPath.row].report_date
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "OpenIncident", sender: self)
    }
    
    
}
