//
//  InjuryInputViewController.swift
//  T02_Orange
//
//  Created by Alexander R Perry on 11/30/18.
//  Copyright © 2018 Austin T Vannoy. All rights reserved.
//

import UIKit

class InjuryInputViewController: UIViewController, UITextFieldDelegate {
    // DatePicker for the incident and report dates
    var datePicker: UIDatePicker?
    var reportdatePicker: UIDatePicker?
    //Outlets for all the fields on the UI
    @IBOutlet weak var DateOfIncidentField: UITextField!
    @IBOutlet weak var LocationOfInjuryOccurenceField: UITextField!
    @IBOutlet weak var ReporterNameField: UITextField!
    @IBOutlet weak var DateOfReportField: UITextField!
    @IBOutlet weak var PotentialHarmField: UITextField!
    @IBOutlet weak var PreventionField: UITextField!
    
    
    @IBOutlet weak var ReportSubmitterField: UITextField!
    @IBOutlet weak var ManagerField: UITextField!
    @IBOutlet weak var ObjectField: UITextField!
    @IBOutlet weak var InjuryLocationField: UITextField!
    @IBOutlet weak var InjurySeverityField: UITextField!
    @IBOutlet weak var DoctorNameField: UITextField!
    @IBOutlet weak var DoctorContactField: UITextField!
    @IBOutlet weak var WitnessField: UITextField!
    
    @IBOutlet weak var AdditionalInfoTextView: UITextView!
    @IBOutlet weak var SafetyPrecautionsTextView: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        DateOfIncidentField.delegate = self
        LocationOfInjuryOccurenceField.delegate = self
        ReporterNameField.delegate = self
        DateOfReportField.delegate = self
        PotentialHarmField.delegate = self
        PreventionField.delegate = self
        ReportSubmitterField.delegate = self
        ManagerField.delegate = self
        AdditionalInfoTextView.delegate = self as? UITextViewDelegate
        ObjectField.delegate = self
        InjuryLocationField.delegate = self
        InjurySeverityField.delegate = self
        DoctorNameField.delegate = self
        DoctorContactField.delegate = self
        WitnessField.delegate = self
        SafetyPrecautionsTextView.delegate = self as? UITextViewDelegate
        datePicker = UIDatePicker()
        reportdatePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        reportdatePicker?.datePickerMode = .date
        DateOfReportField.inputView = reportdatePicker
        DateOfIncidentField.inputView = datePicker
        datePicker?.addTarget(self, action: #selector(NonInputViewController.dateChanged(datePicker:)), for: .valueChanged)
        reportdatePicker?.addTarget(self, action: #selector(NonInputViewController.reportdateChanged(reportdatePicker:)), for: .valueChanged)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(NonInputViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
        
    }
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        DateOfIncidentField.text = dateFormatter.string(from: datePicker.date)
        
    }
    @objc func reportdateChanged(reportdatePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        DateOfReportField.text = dateFormatter.string(from: reportdatePicker.date)
        
    }
    
    @IBAction func submitButton(_ sender: Any) {
        //performSegue(withIdentifier: "InjuryType", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier=="InjuryType")
        {
            let destVc : NewIncidentViewController = segue.destination as! NewIncidentViewController
            
            
            destVc.typeOfIncident = "Injury"
            
            destVc.iDate = DateOfIncidentField.text ?? "N/A"
            destVc.iLocation = LocationOfInjuryOccurenceField.text ?? "N/A"
            destVc.iReporter = ReporterNameField.text ?? "N/A"
            destVc.iReportDate = DateOfReportField.text ?? "N/A"
            destVc.iPotential = PotentialHarmField.text ?? "N/A"
            destVc.iPrevention = PreventionField.text ?? "N/A"
            print("Trying")
            //let te = AdditionalInfoTextView.text
            print(AdditionalInfoTextView.text)
            destVc.iAdditional = AdditionalInfoTextView.text ?? "N/A"
            destVc.iSubmitter = ReportSubmitterField.text ?? "N/A"
            destVc.iManager = ManagerField.text ?? "N/A"
            destVc.iObject = ObjectField.text ?? "N/A"
            destVc.iBody = InjuryLocationField.text ?? "N/A"
            destVc.iSeverity = InjurySeverityField.text ?? "N/A"
            destVc.iDoctor = DoctorNameField.text ?? "N/A"
            destVc.iDoctorInfo = DoctorContactField.text ?? "N/A"
            destVc.iWitness = WitnessField.text ?? "N/A"
            destVc.iPrecautions = SafetyPrecautionsTextView.text ?? "N/A"
            
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        DateOfIncidentField.resignFirstResponder()
        LocationOfInjuryOccurenceField.resignFirstResponder()
        ReporterNameField.resignFirstResponder()
        DateOfReportField.resignFirstResponder()
        PotentialHarmField.resignFirstResponder()
        PreventionField.resignFirstResponder()
        ReportSubmitterField.resignFirstResponder()
        ManagerField.resignFirstResponder()
        AdditionalInfoTextView.resignFirstResponder()
        ObjectField.resignFirstResponder()
        InjuryLocationField.resignFirstResponder()
        InjurySeverityField.resignFirstResponder()
        DoctorNameField.resignFirstResponder()
        DoctorContactField.resignFirstResponder()
        WitnessField.resignFirstResponder()
        SafetyPrecautionsTextView.resignFirstResponder()
        return true
    }
    
}
