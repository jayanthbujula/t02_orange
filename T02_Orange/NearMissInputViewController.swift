//
//  NearMissInputViewController.swift
//  T02_Orange
//
//  Created by Alexander R Perry on 11/30/18.
//  Copyright © 2018 Austin T Vannoy. All rights reserved.
//

import UIKit

class NearMissInputViewController: UIViewController, UITextFieldDelegate {
    //Outlets for all the fields on the UI
    @IBOutlet weak var DateOfIncidentField: UITextField!
    @IBOutlet weak var LocationOfInjuryOccurenceField: UITextField!
    @IBOutlet weak var ReporterNameField: UITextField!
    @IBOutlet weak var DateOfReportField: UITextField!
    @IBOutlet weak var PotentialHarmField: UITextField!
    @IBOutlet weak var PreventionField: UITextField!
    
    
    @IBOutlet weak var ReportSubmitterField: UITextField!
    @IBOutlet weak var ManagerField: UITextField!
    
    @IBOutlet weak var AdditionalInfoTextView: UITextView!
    // DatePicker for the incident and report dates
    var datePicker: UIDatePicker?
    var reportdatePicker: UIDatePicker?
    override func viewDidLoad() {
        super.viewDidLoad()
        DateOfIncidentField.delegate = self
        LocationOfInjuryOccurenceField.delegate = self
        ReporterNameField.delegate = self
        DateOfReportField.delegate = self
        PotentialHarmField.delegate = self
        PreventionField.delegate = self
        ReportSubmitterField.delegate = self
        ManagerField.delegate = self
        AdditionalInfoTextView.delegate = self as? UITextViewDelegate
        datePicker = UIDatePicker()
        reportdatePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        reportdatePicker?.datePickerMode = .date
        DateOfReportField.inputView = reportdatePicker
        DateOfIncidentField.inputView = datePicker
        datePicker?.addTarget(self, action: #selector(NonInputViewController.dateChanged(datePicker:)), for: .valueChanged)
        reportdatePicker?.addTarget(self, action: #selector(NonInputViewController.reportdateChanged(reportdatePicker:)), for: .valueChanged)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(NonInputViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
        
    }
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        DateOfIncidentField.text = dateFormatter.string(from: datePicker.date)
        
    }
    @objc func reportdateChanged(reportdatePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        DateOfReportField.text = dateFormatter.string(from: reportdatePicker.date)}
    // Segue to the NewIncident form by sending all the form data
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier=="NearMissType")
        {
            let destVc : NewIncidentViewController = segue.destination as! NewIncidentViewController
          
            destVc.typeOfIncident = "Near-Miss"
            
            destVc.iDate = DateOfIncidentField.text ?? "N/A"
            destVc.iLocation = LocationOfInjuryOccurenceField.text ?? "N/A"
            destVc.iReporter = ReporterNameField.text ?? "N/A"
            destVc.iReportDate = DateOfReportField.text ?? "N/A"
            destVc.iPotential = PotentialHarmField.text ?? "N/A"
            destVc.iPrevention = PreventionField.text ?? "N/A"
            
            destVc.iAdditional = AdditionalInfoTextView.text ?? "N/A"
            destVc.iSubmitter = ReportSubmitterField.text ?? "N/A"
            destVc.iManager = ManagerField.text ?? "N/A"
            
            destVc.iObject = ""
            destVc.iBody = ""
            destVc.iSeverity = ""
            destVc.iDoctor = ""
            destVc.iDoctorInfo = ""
            destVc.iWitness = ""
            destVc.iPrecautions = ""
           
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        DateOfIncidentField.resignFirstResponder()
        LocationOfInjuryOccurenceField.resignFirstResponder()
        ReporterNameField.resignFirstResponder()
        DateOfReportField.resignFirstResponder()
        PotentialHarmField.resignFirstResponder()
        PreventionField.resignFirstResponder()
        ReportSubmitterField.resignFirstResponder()
        ManagerField.resignFirstResponder()
        AdditionalInfoTextView.resignFirstResponder()
        
        return true
    }
    
}
