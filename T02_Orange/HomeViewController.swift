//
//  HomeViewController.swift
//  T02_Orange
//
//  Created by Alexander R Perry on 11/30/18.
//  Copyright © 2018 Austin T Vannoy. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    // Injury button action
    @IBAction func InjuryButton(_ sender: Any) {
        //performSegue(withIdentifier: "InjurySegueForm", sender: self)
    }
    // Near-Injury button action
    @IBAction func NearButton(_ sender: Any) {
    //performSegue(withIdentifier: "NearSegueForm", sender: self)
    }
    // Non-Injury button action
    @IBAction func NonButton(_ sender: Any) {
        //performSegue(withIdentifier: "NonSegueForm", sender: self)
    }
    
    @IBAction func unwindToMenu(segue:UIStoryboardSegue) { }

}
