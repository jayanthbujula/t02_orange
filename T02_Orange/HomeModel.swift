//
//  HomeModel.swift
//  T02_Orange
//
//  Created by Jayanth Bujula on 12/1/18.
//  Copyright © 2018 Austin T Vannoy. All rights reserved.
//

import UIKit
protocol HomeModelDelegate {
    func itemsDownloaded(incidents:[datavalues])
}
class HomeModel: NSObject {
    var delegate:HomeModelDelegate?
    func getItems(_ str: String){
        
        let serviceURL = "http://cs.okstate.edu/~jbujula/incident_service.php/"
        
        let url = URL(string: serviceURL)
        if let url = url{
            let session = URLSession(configuration: .default)
            
            let task = session.dataTask(with: url, completionHandler: {
                (data, response, error) in
                if error == nil {
                    self.parseJson(data!, str)
                } else {
                    
                }
            })
            // start the task
            task.resume()
        }
        
    }
    func parseJson(_ data: Data, _ str: String){
        var dataArray = [datavalues]()
        do{
            let jsonarray = try JSONSerialization.jsonObject(with: data, options: []) as! [Any]
            for jsonResult in jsonarray{
                let jsonDict = jsonResult as! [String: String]
                if jsonDict["incident_label"]==str{
                    let data = datavalues(id: jsonDict["id"]!, incident_type: jsonDict["incident_type"] ?? " ", location: jsonDict["location"] ?? " ", reporter_name: jsonDict["reporter_name"] ?? " ", report_date: jsonDict["report_date"] ?? " ", incident_description: jsonDict["incident_description"] ?? " ", potential_harm: jsonDict["potential_harm"] ?? " ", incident_prevention: jsonDict["incident_prevention"] ?? " ", additional_info: jsonDict["additional_info"] ?? " ", manager_name: jsonDict["manager_name"] ?? " ", injuring_object: jsonDict["injuring_object"] ?? " ", injury_location: jsonDict["injury_location"] ?? " ", injury_severity: jsonDict["injury_severity"] ?? " ", doctor_visited: jsonDict["doctor_visited"] ?? " ", doctor_contact_info: jsonDict["doctor_contact_info"] ?? " ", incident_witness: jsonDict["incident_witness"] ?? " ",  safety_procedures: jsonDict["safety_procedures"] ?? " ", incident_label: jsonDict["incident_label"] ?? " ", incident_date: jsonDict["incident_date"] ?? " ", report_submitter: jsonDict["report_submitter"] ?? " "  )
                    dataArray.append(data)
                }}
            DispatchQueue.main.async {
                self.delegate?.itemsDownloaded(incidents: dataArray)
            }
            
        } catch{
            print("error")
        }
    }
}

