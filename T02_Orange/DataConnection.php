<?php

class DataBaseConnect
{
    private $con;

    function __construct()
    {
    }

    function connect()
    {
    
        require_once 'Config.php';

        $this->con = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

        // Check connection issue
        if (mysqli_connect_errno()) {
            echo "MySQL CONNECTION: " . mysqli_connect_error();
        }

        // returing connection resource
        return $this->con;
    }
}