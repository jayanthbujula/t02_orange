//
//  NewIncidentViewController.swift
//  T02_Orange
//
//  Created by Austin T Vannoy on 10/30/18.
//  Copyright © 2018 Austin T Vannoy. All rights reserved.
//

import UIKit

class NewIncidentViewController: UIViewController {
    //Outlets for all the fields on the UI
    @IBOutlet weak var TitleInjury: UILabel!
    @IBOutlet weak var TitleNon: UILabel!
    @IBOutlet weak var TitleNear: UILabel!
    
    @IBOutlet weak var LabelObject: UILabel!
    @IBOutlet weak var LabelInjuryLoc: UILabel!
    @IBOutlet weak var LabelInjurySeverity: UILabel!
    @IBOutlet weak var LabelDoctor: UILabel!
    @IBOutlet weak var LabelDoctorInfo: UILabel!
    @IBOutlet weak var LabelWitness: UILabel!
    @IBOutlet weak var ManagerName: UILabel!
    @IBOutlet weak var LabelSafetyPrecautions: UILabel!
    //var newIncident : Incident!
    @IBOutlet weak var DateOfIncident: UILabel!
    @IBOutlet weak var LocationOfIncident: UILabel!
    @IBOutlet weak var ReporterName: UILabel!
    @IBOutlet weak var DateOfReport: UILabel!
    @IBOutlet weak var PotentialHarm: UILabel!
    @IBOutlet weak var Prevention: UILabel!
    //@IBOutlet weak var AddInfo: UILabel!
    @IBOutlet weak var SubmitterName: UILabel!
    
    @IBOutlet weak var ManagerN: UILabel!
    @IBOutlet weak var ObjectCauseHarm: UILabel!
    @IBOutlet weak var BodyPartInjured: UILabel!
    @IBOutlet weak var InjurySeverity: UILabel!
    @IBOutlet weak var DoctorName: UILabel!
    @IBOutlet weak var DoctorInfo: UILabel!
    @IBOutlet weak var Witness: UILabel!
    //@IBOutlet weak var SafetyPrecautions: UILabel!
    @IBOutlet weak var AddInfo: UITextView!
    @IBOutlet weak var SafetyPrecautions: UITextView!
    
    
    let serviceURL = "http://cs.okstate.edu/~jbujula/incident_service.php/"
    
    //Note the iDate = incident Date. i is abbreviated for incident simplicity
    var typeOfIncident = "" // pass this from previous page use to hide proper header
    var iDate = "N/A" //2018-12-01
    var iLocation = "N/A"
    var iReporter = "N/A"
    var iReportDate = "N/A" //2018-12-01
    var iPotential = "N/A"
    var iPrevention = "N/A"
    var iAdditional = "N/A"
    var iSubmitter = "N/A"
    var iManager = "N/A"
    var iObject = "N/A"
    var iBody = "N/A"
    var iSeverity = "N/A"
    var iDoctor = "N/A"
    var iDoctorInfo = "N/A"
    var iWitness = "N/A"
    var iPrecautions = "N/A"
    var iTest = "N/A"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if(typeOfIncident=="Injury")
        {
            //unhide everything and set everything
            TitleInjury.isHidden = false
            TitleNon.isHidden = true
            TitleNear.isHidden = true
            ObjectCauseHarm.isHidden = false
            BodyPartInjured.isHidden = false
            InjurySeverity.isHidden = false
            DoctorName.isHidden = false
            DoctorInfo.isHidden = false
            Witness.isHidden = false
            SafetyPrecautions.isHidden = false
            
            //unhide labels
            LabelObject.isHidden = false
            LabelInjuryLoc.isHidden = false
            LabelDoctor.isHidden = false
            LabelWitness.isHidden = false
            LabelDoctorInfo.isHidden = false
            LabelInjurySeverity.isHidden = false
            LabelSafetyPrecautions.isHidden = false
            //
            DateOfIncident.text = iDate //String(iDate)
            LocationOfIncident.text = iLocation
            LocationOfIncident.sizeToFit()
            ReporterName.text = iReporter
            DateOfReport.text = iReportDate //String(iReportDate)
            PotentialHarm.text = iPotential
            Prevention.text = iPrevention
            AddInfo.text = iAdditional
            //AddInfo.sizeToFit()
            SubmitterName.text = iSubmitter
            ManagerN.text = iManager
            ObjectCauseHarm.text = iObject
            BodyPartInjured.text = iBody
            InjurySeverity.text = iSeverity
            DoctorName.text = iDoctor
            DoctorInfo.text = iDoctorInfo
            Witness.text = iWitness
            SafetyPrecautions.text = iPrecautions
            
            
        }
        else if(typeOfIncident=="Non-Injury")
        {
            TitleInjury.isHidden = true
            TitleNon.isHidden = false
            TitleNear.isHidden = true
            ObjectCauseHarm.isHidden = true
            BodyPartInjured.isHidden = true
            InjurySeverity.isHidden = true
            DoctorName.isHidden = true
            DoctorInfo.isHidden = true
            Witness.isHidden = true
            //SafetyPrecautions.isHidden = true
            
            LabelObject.isHidden = true
            LabelInjuryLoc.isHidden = true
            LabelDoctor.isHidden = true
            LabelWitness.isHidden = true
            LabelDoctorInfo.isHidden = true
            LabelInjurySeverity.isHidden = true
            LabelSafetyPrecautions.isHidden = true
            
            DateOfIncident.text = iDate //String(iDate)
            LocationOfIncident.text = iLocation
            LocationOfIncident.sizeToFit()
            ReporterName.text = iReporter
            DateOfReport.text = iReportDate //String(iReportDate)
            PotentialHarm.text = iPotential
            Prevention.text = iPrevention
            AddInfo.text = iAdditional
            //AddInfo.sizeToFit()
            SubmitterName.text = iSubmitter
            ManagerN.text = iManager
        }
        else if(typeOfIncident=="Near-Miss")
        {
            TitleInjury.isHidden = true
            TitleNon.isHidden = true
            TitleNear.isHidden = false
            ObjectCauseHarm.isHidden = true
            BodyPartInjured.isHidden = true
            InjurySeverity.isHidden = true
            DoctorName.isHidden = true
            DoctorInfo.isHidden = true
            Witness.isHidden = true
            //SafetyPrecautions.isHidden = true
            
            LabelObject.isHidden = true
            LabelInjuryLoc.isHidden = true
            LabelDoctor.isHidden = true
            LabelWitness.isHidden = true
            LabelDoctorInfo.isHidden = true
            LabelInjurySeverity.isHidden = true
            LabelSafetyPrecautions.isHidden = true
            
            DateOfIncident.text = iDate //String(iDate)
            LocationOfIncident.text = iLocation
            LocationOfIncident.sizeToFit()
            ReporterName.text = iReporter
            DateOfReport.text = iReportDate //String(iReportDate)
            PotentialHarm.text = iPotential
            Prevention.text = iPrevention
            AddInfo.text = iAdditional
            //AddInfo.sizeToFit()
            SubmitterName.text = iSubmitter
            ManagerN.text = iManager
        }
    }
    
    
    @IBAction func submit(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Confirm?", message: "Do you want to submit the incident?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Accept", style: .default, handler: {
            action in
            //created NSURL
            let requestURL = NSURL(string: self.serviceURL)
            
            //creating NSMutableURLRequest
            let request = NSMutableURLRequest(url: requestURL! as URL)
            
            //setting the method to post
            request.httpMethod = "POST"
            
            //creating the post parameter by concatenating the keys and values from text field
            let incident_type = self.typeOfIncident
            let incident_date = self.iDate
            let location = self.iLocation
            let reporter_name = self.iReporter
            let report_date = self.iReportDate
            let potential_harm = self.iPotential
            let incident_prevention = self.iPrevention
            let additional_info = self.iAdditional
            let report_submitter = self.iSubmitter
            let manager_name = self.iManager
            let injuring_object = self.iObject
            let injury_location = self.iBody
            let injury_severity = self.iSeverity
            let doctor_visited = self.iDoctor
            let doctor_contact_info = self.iDoctorInfo
            let incident_witness = self.iWitness
            let safety_procedures = self.iPrecautions
            let incident_label = "open"
            
            //adding the parameters to request body
            let postParameters = "incident_type="+incident_type+"&incident_date="+incident_date+"&location="+location+"&reporter_name="+reporter_name+"&report_date="+report_date+"&potential_harm="+potential_harm+"&incident_prevention="+incident_prevention+"&additional_info="+additional_info+"&report_submitter="+report_submitter+"&manager_name="+manager_name+"&injuring_object="+injuring_object+"&injury_location="+injury_location+"&injury_severity="+injury_severity+"&doctor_visited="+doctor_visited+"&doctor_contact_info="+doctor_contact_info+"&incident_witness="+incident_witness+"&safety_procedures="+safety_procedures+"&incident_label="+incident_label;
            
            request.httpBody = postParameters.data(using: String.Encoding.utf8)
            
            //creating a task to send the post request
            let task = URLSession.shared.dataTask(with: request as URLRequest){
                data, response, error in
                
                if error != nil{
                    return;
                }
                
                //parsing the response
                do {
                    //converting resonse to NSDictionary
                    let myJSON =  try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                    
                    //parsing the json
                    if let parseJSON = myJSON {
                        
                        //creating a string
                        var msg : String!
                        
                        //getting the json response
                        msg = parseJSON["message"] as! String?
                        
                        //printing the response
                        print(msg)
                        
                    }
                } catch {
                    print(error)
                }
                
            }
            //executing the task
            task.resume()
            
            self.performSegue(withIdentifier: "UnwindToMenuFromNew", sender: self)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(action)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        
    }
}
