////
////  Incident.swift
////  T02_Orange
////
////  Created by Austin T Vannoy on 11/8/18.
////  Copyright � 2018 Austin T Vannoy. All rights reserved.
////
//
//import Foundation
//
//class Incident
//{
//    // Data Source for Incident Data
//    var dataSource: [IData] = []
//    
//    var appDelegate: AppDelegate?
//    var context: NSManagedObjectContext?
//    var fetchRequest: NSFetchRequest<IData>!
//    
//    var reportClassification: Int // 0-Injury 1-Near Miss 2-No Injury
//
//    var incidentDate = ""
//    var incidentLocation = ""
//    var incidentReporter = ""
//    var reportDate = ""
//    
//    init() {
//        appDelegate = UIApplication.shared.delegate as? AppDelegate
//        context = appDelegate?.persistentContainer.viewContext
//        fetchRequest = Course.fetchRequest()
//        
//        // Set Predicate so Everything is Requested
//        fetchRequest.predicate = nil
//        
//        // Fetch Database Contents
//        do {
//            dataSource = try context?.fetch(fetchRequest) ?? []
//        }
//        catch let error as NSError {
//            print("Cannot load data: \(error)")
//        }
//        
//        // Reset to NIL to Prevent Issues if Used Later
//        fetchRequest.predicate = nil
//        fetchRequest.sortDescriptors = []
//    }
//    func saveData(data: IData) {
//        // Update the Data Store
//            try context?.save()
//            dataSource.append(data)
//    }
//}
